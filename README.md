# Open in VSCodium

A Finder toolbar button to open files or folders in VSCodium on MacOS.

Update: The VSCodium team has changed the logo the latest change involves just the logo change.

![Finder toolbar](img/vscodium.png)

The "Open in VSCodium"-applet is made with Automator.

![Automator screenshot](img/automator_screenshot.png)

## Add the app to the Finder Toolbar

To add the "Open in VSCodium" app to the Finder toolbar:

- Download the .zip file
- Unpack the zip file
- Drag the applet to the toolbar while holding the command-key (&#x2318;).

It should look like the image below.

![Open in VSCodium in the Finder toolbar](img/finder_toolbar_light.png) or with the dark theme
![Open in VSCodium in the Finder toolbar](img/finder_toolbar_dark.png)

I have not found a way to change the toolbar button there for this compromise of a toolbar button.

Now you can try it out, select a folder or file and click on the new button to open VSCodium with the selected items(s).
